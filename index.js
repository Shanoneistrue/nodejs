const http = require('http')
// to read the html file (index.html)
const fs = require('fs')


// port we wanted to listen at.
const port = 1500


// createServer function creates a server and it required two functions one for request and other for response.
const server = http.createServer(function(req, res) {
// request is called everytime someone calles this server to return a web content.
// first parameter is status code and the other one is a headers. text/html tells browser that the response if html and it should parse it as html doc.
res.writeHead(200, { 'Content-Type': 'text/html'})
fs.readFile('index.html', function(error, data) {
  if (error) {
    res.writeHead(404)
    res.write('Error: File Not Found')
  } else {
    res.write(data)
  }
  res.end()
})
})

// setting up server to listen on the port that we wanted to. listen func accepts port number and a function to handle errors.
server.listen(port, function(error) {
  if (error) {
    console.log("Something went wrong:", error)
  } else {
    console.log('Server is listening on port:', port)
  }
})



// https://stackoverflow.com/questions/9164915/node-js-eacces-error-when-listening-on-most-ports
// https://github.com/nodejs/help/issues/1159
// got above error when tried to set server to listen at port 1000
